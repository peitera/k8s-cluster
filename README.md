[![pipeline status](https://gitlab.com/peitera/k8s-cluster/badges/dev/pipeline.svg)](https://gitlab.com/peitera/k8s-cluster/-/commits/dev)

# k8s-cluster
k8s cluster proj

### requirements

- ansible
- terraform
- python
- boto
- aws cli

### What it does

- creates aws infrastructure defined in the `/terraform` directory
- uses dynamic inventory from `/inventory` to populate the Ansible inventory
- hosts are defined in two groups `workers` and `master`
- refreshes inventory after terraform completes
- installs pre-req, docker and k8s on master nodes
- creates cluster join token
- installs pre-req, docker and k8s on worker nodes
- uses token to join worker nodes to cluster

### gitlab-ci

### stages:
- test
  - linting on Ansible playbook and roles referenced in playbooks/main.yml.
  - terraform validate via `validate` and formatting via `terraform fmt`
- plan
  - performs `terraform plan`, saves output as `planfile`
  - also converts plan to json as `planfile.json` and saves both as artifacts
- deploy
  - master only, runs `terraform apply`, this is a manual step.
- post
  - runs playbooks to configure post deployment, this will only run if deploy has.
- AWS credentials need to be stored as a variable in gitlab 
